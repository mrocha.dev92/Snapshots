package com.example.snapshots.utils

import android.view.View

/** Las funciones de extención nos sirven para simplificar nuestro código y si es
 * alguna función que se utiliza en diferentes lugares, entonces poder reutilizarla
 * sin tener que escribirla varias veces*/


fun View.show(){
    this.visibility = View.VISIBLE
}
fun View.hide(){
    this.visibility = View.GONE
}
fun View.invisible(){
    this.visibility = View.INVISIBLE
}

fun View.enable(){
    this.isEnabled = true
}
fun View.disable(){
    this.isEnabled = false
}

