package com.example.snapshots

import android.app.Activity
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.provider.MediaStore
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import com.example.snapshots.databinding.FragmentAddBinding
import com.example.snapshots.utils.*
import com.google.android.material.snackbar.Snackbar
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.storage.FirebaseStorage
import com.google.firebase.storage.StorageReference

class AddFragment : Fragment() {


    private lateinit var mStorageReferences: StorageReference
    private lateinit var mDatabaseReference: DatabaseReference

    private lateinit var mBinding: FragmentAddBinding
    private var mPhotoSelectUri: Uri? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        mBinding = FragmentAddBinding.inflate(inflater, container, false)
        return mBinding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        /** Agregamos evento a nuestros botones*/
        mBinding.btPost.setOnClickListener {
            if (mBinding.etTitle.text.toString().isEmpty()) {
                Toast.makeText(context, R.string.should_add_title, Toast.LENGTH_SHORT).show()
            } else {
                postSnapshot()
            }
        }
        mBinding.btnSelect.setOnClickListener { openGallery() }


        /** Inicializamos nuestras variables */
        mStorageReferences = FirebaseStorage.getInstance().reference
        mDatabaseReference = FirebaseDatabase.getInstance().reference.child(PATH_SNAPSHOT)
    }

    /** Abre la galería */
    private fun openGallery() {
        val intent = Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI)
        startActivityForResult(intent, RC_GALLERY)
    }

    /** Subimos imagen a storage de firebase*/
    private fun postSnapshot() {
        mBinding.progressBar.show()


        val key = mDatabaseReference.push().key!!
        val uid = FirebaseAuth.getInstance().currentUser!!.uid

        val storageReferences = mStorageReferences
            .child(PATH_SNAPSHOT)
            .child(uid)
            .child(key)


        if (mPhotoSelectUri != null) {
            storageReferences.putFile(mPhotoSelectUri!!)
                .addOnProgressListener {
                    val progress = (100 * it.bytesTransferred / it.totalByteCount).toDouble()
                    mBinding.progressBar.progress = progress.toInt()
                    mBinding.tvMessage.text = "$progress %"
                }
                .addOnCompleteListener {
                    mBinding.progressBar.invisible()
                }
                .addOnSuccessListener {
                    Snackbar.make(mBinding.root, "Instantánea publicada", Snackbar.LENGTH_SHORT)
                        .show()
                    it.storage.downloadUrl.addOnSuccessListener { uri ->
                        saveSnapshot(key, uri.toString(), mBinding.etTitle.text.toString().trim())
                    }
                }
                .addOnFailureListener {
                    Snackbar.make(
                        mBinding.root,
                        "No se pudo subir, intente mas tarde",
                        Snackbar.LENGTH_SHORT
                    ).show()
                }
        } else {
            Snackbar.make(
                mBinding.root,
                "No se pudo subir la imagen, intente mas tarde",
                Snackbar.LENGTH_SHORT
            ).show()
        }
    }

    /** Guardamos url de la imagen en base de datos */
    private fun saveSnapshot(key: String, url: String, title: String) {
        val snapshot = Snapshot(title = title, photoUrl = url)
        mDatabaseReference.child(key).setValue(snapshot)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == RC_GALLERY) {
                mPhotoSelectUri = data?.data
                mBinding.imPhoto.setImageURI(mPhotoSelectUri)
                mBinding.tilTitle.show()
                mBinding.tvMessage.text = getString(R.string.post_message_valid_title)
            }
        }
    }
}