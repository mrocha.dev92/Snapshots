package com.example.snapshots

import android.content.Context
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.example.snapshots.databinding.FragmentHomeBinding
import com.example.snapshots.databinding.ItemSnapshotBinding
import com.example.snapshots.utils.LIKE_LIST
import com.example.snapshots.utils.PATH_SNAPSHOT
import com.example.snapshots.utils.hide
import com.firebase.ui.database.FirebaseRecyclerAdapter
import com.firebase.ui.database.FirebaseRecyclerOptions
import com.firebase.ui.database.SnapshotParser
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase

class HomeFragment : Fragment(), HomeAux {


    private lateinit var mBinding: FragmentHomeBinding
    private lateinit var mFirebaseAdapter: FirebaseRecyclerAdapter<Snapshot, SnapshotHolder>
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        mBinding = FragmentHomeBinding.inflate(inflater, container, false)
        return  mBinding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        /** Obtiene la ruta base donde se almacenan los datos */
        val query = FirebaseDatabase.getInstance().reference.child(PATH_SNAPSHOT)

        val option =
        FirebaseRecyclerOptions.Builder<Snapshot>().setQuery(query) {
            val snapshot = it.getValue(Snapshot::class.java)
            snapshot!!.id = it.key!!
            snapshot
        }.build()


        /**  Se inicialzia el dapter*/
        mFirebaseAdapter = object : FirebaseRecyclerAdapter<Snapshot, SnapshotHolder>(option){

            private lateinit var mContext: Context
            override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SnapshotHolder {
                mContext = parent.context

                val view = LayoutInflater.from(mContext)
                    .inflate(R.layout.item_snapshot, parent, false)
                return  SnapshotHolder(view)
            }

            override fun onBindViewHolder(holder: SnapshotHolder, position: Int, model: Snapshot) {

                val snapshot = getItem(position)
                with(holder){
                    setListener(snapshot)

                    binding.tvTitle.text = snapshot.title
                    binding.cbLike.text = snapshot.likeList.keys.size.toString() // para saber cuantos likes tiene la imagen



                    /**Obtenemos el UID del usaurio autenticado y revisamos si este UID está
                     * registrado en la lista de likes de la foto qe se encuentre en esta posición
                     * del arreglo*/
                    FirebaseAuth.getInstance().currentUser?.let {
                        binding.cbLike.isChecked = snapshot.likeList
                            .containsKey(it.uid)
                    }

                    Glide.with(mContext)
                        .load(snapshot.photoUrl)
                        .diskCacheStrategy(DiskCacheStrategy.ALL)
                        .centerCrop()
                        .into(binding.imgPhoto)
                }
            }

            override fun onDataChanged() {
                super.onDataChanged()
                mBinding.progressBar.hide()
            }

            override fun onError(error: DatabaseError) {
                super.onError(error)
                Toast.makeText(mBinding.progressBar.context, error.message, Toast.LENGTH_SHORT).show()
            }
        }

        /** Asignamos el adaptador a nuestro recyclerView */
        mBinding.recylerView.apply {
            setHasFixedSize(true)
            adapter = mFirebaseAdapter
        }
    }

    /** Desde que se inicia nuestro fragmento le indicamos al adapter que
     *  comience a escuchar si hay un cambio en la base de datos*/
    override fun onStart() {
        super.onStart()
        mFirebaseAdapter.startListening()
    }

    /** Detenemos el listener de nuestro adapter */
    override fun onStop() {
        super.onStop()
        mFirebaseAdapter.stopListening()
    }

    override fun gotToTop() {
        mBinding.recylerView.smoothScrollToPosition(0)
    }

    /** Función para eliminar un registro de la base de datos*/
    private fun deleteSnapshot(snapshot: Snapshot){
        val databaseReference = FirebaseDatabase.getInstance().reference.child(PATH_SNAPSHOT)
        databaseReference.child(snapshot.id).removeValue()

    }

    /** Aquí se inserta el UID del usuario dentro de la lista de likes
     * que se encuentra en el registro del la fotografía a la que dicho user
     * le dió like
     *  */
    private fun setLike(snapshot: Snapshot, checked: Boolean){
        val databaseReference = FirebaseDatabase.getInstance().reference.child(PATH_SNAPSHOT)

        /** Agrega el like a la foto*/
        if (checked){
            databaseReference.child(snapshot.id).child(LIKE_LIST)
                .child(FirebaseAuth.getInstance().currentUser!!.uid).setValue(checked)
        }else{

            /** Elimina el like de la foto*/
            databaseReference.child(snapshot.id).child(LIKE_LIST)
                .child(FirebaseAuth.getInstance().currentUser!!.uid).setValue(null)
        }
    }

    /**  Esta clase manda llamar la acción que se detona cuando se da click a un botón
     * en este caso el botón de eliminar y el de dar un like */
    inner class SnapshotHolder(view: View): RecyclerView.ViewHolder(view){
        val binding = ItemSnapshotBinding.bind(view)
        fun setListener (snapshot: Snapshot){
            binding.btnDelete.setOnClickListener { deleteSnapshot(snapshot) }
            binding.cbLike.setOnCheckedChangeListener { buttonView, isChecked ->
                setLike(snapshot, isChecked)
            }
        }
    }



}